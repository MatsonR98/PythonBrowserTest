import webbrowser, sys, random, time, psutil

def main():
    USER_TEST_ADDRESS = ""
    RANDOM_SITES = ("www.google.com", "www.amazon.com", "www.youtube.com", "www.reddit.com")
    NUM_TABS = 10

    try:
        for i in range(0, len(sys.argv)):
            if(sys.argv[i] == "-a"):
                USER_TEST_ADDRESS = str(sys.argv[i+1])
                print("Address: "+ USER_TEST_ADDRESS)
            elif(sys.argv[i] == "-n"):
                NUM_TABS = int(sys.argv[i+1])
        print("Number of tabs to open: "+str(NUM_TABS))
        pass

    except IndexError as invalid_index:
        print("Invalid arguments entered. Closing...")
        exit(0)

    input("Press any key to start.")

    if(USER_TEST_ADDRESS!=""):
        n = 0
        while (n<NUM_TABS):
            open_browser_tab(USER_TEST_ADDRESS)
            print(str(n+1)+" tabs opened.")
            print("Memory usage is: "+str(psutil.virtual_memory()[2])+"%")
            if(n==0):
                time.sleep(5)
            n += 1
    elif(USER_TEST_ADDRESS==""):
        n = 0
        while(n<NUM_TABS):
            v = random.randint(0,3)
            open_browser_tab(RANDOM_SITES[v])
            print(str(n+1)+" tabs opened. Current tab is "+ RANDOM_SITES[v])
            print("Memory usage is: "+str(psutil.virtual_memory()[2])+"%")
            if(n==0):
                time.sleep(5)
            n += 1
    
    prev_pct = 0.0
    cur_pct = 0.0
    print("Please wait until all tabs have completely loaded, then press Ctrl-C to see peak memory usage.")

    try:
        while(cur_pct>=(prev_pct-0.5)):
            prev_pct = cur_pct
            cur_pct = float(psutil.virtual_memory()[2])
        pass
    except KeyboardInterrupt as kbd:
        print("Peak memory usage = "+str(cur_pct) + "%")
        quit(0)

    


def open_browser_tab(addr):
    time.sleep(0.1)
    webbrowser.open(addr, 0, False)

if __name__ == "__main__":
    print("""\n-----------------------------------------------
Python Browser Tab Test, created by Ryan Matson
-----------------------------------------------\n""")
    print("\nUse the \'a\' flag to specify an address to load. Otherwise, random general sites are used.\nUse \'-n\' to specify the number of tabs to test. Default is 10")
    try:
        main()
        pass
    except Exception as exc:
        print(str(type(exc)))
        pass
    